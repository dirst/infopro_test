<?php
class Path
{
    public $currentPath;

    function __construct($path)
    {
        $this->currentPath = $path;
    }

    public function cd($newPath)
    {
      $cPath = ltrim($this->currentPath, "/");
      $cPath = explode("/", $cPath);
      $path = explode("/", ltrim($newPath, "/"));

      foreach ($path as $pos => $one) {
        if ($pos == 0 && $newPath[0] == "/") {
          $cPath = [];
        }
        
        if ($one == ".." && !empty($cPath)) {
          array_pop($cPath);
        } elseif ($one != '..') {
          array_push($cPath, $one);
        }
      }
      
      $this->currentPath = "/" . implode("/", $cPath);
    }
}

$path = new Path('/a/b/c/d');
$path->cd('/x');
echo $path->currentPath;