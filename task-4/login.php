<?php
  define("LOGIN", "dima");
  define("PASS", "pass");

  session_start();
  
  if (isset($_POST['submit']) && $_POST['login'] == LOGIN && $_POST['pass'] == PASS) {
    $_SESSION['auth'] = LOGIN;
  } elseif (isset($_POST['submit'])) {
    $msg = "Wrong credentials";
  } elseif (isset($_POST['logout'])) {
    unset($_SESSION['auth']);
    session_destroy();
  }
  
  if (isset($_SESSION['auth'])) {
    $msg = "Hello " . $_SESSION['auth'];
  }
  
?>
<!doctype html>
<html>
  <head>
    <title>SIMPLE LOGIN</title>  
  </head>
  <body>
    <?php print $msg; ?>
    <?php if ($_SESSION['auth']) :?>
      <form method="post">
        <input type="submit" value="logout" name="logout">
      </form>
    <?php else: ?>
      <form method="post">
        <input required type="text" placeholder="login" name="login">
        <input required type="password" placeholder="password" name="pass">
        <input type="submit" value="login" name="submit">
      </form>
    <?php endif; ?>

  </body>
</html>