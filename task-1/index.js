/**
 * Will not work correctly if substring will appear twice or more.
 */
function solve(st, a, b){
  var sub = st.substring(a, b + 1);
  
  st = st.replace(sub, sub.split("").reverse().join(""));
  return st;
}

function solve2(st, a, b){
  var sub = st.substring(a, b + 1).split("");
  var split = st.split("");

  for (var i = 0; i < st.length; i++) {
    if (i >= a && i <= b) {
      split[i] = sub.pop();
    }
  }

  return split.join("");
}